################################################################################
#
#       Author:         Dylan Bayley
#       Email:          dylan.bayley@team.telstra.com
#
#       File:           perceptron_model.py
#       Description:    Source code for Machine Learning Asssignment
#
################################################################################
import sys
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
import pandas as pd

def load_training_data(data_set):
    """
    This function takes the name of a desired dataset and it loads it from the sklearn library
    :param data_set: string - name of dataset (iris only at this point in time)
    :return: variable called input_attr, and target_values
    """

    # preprocess data_set var for spaces and quotes.
    data_set = data_set.replace("'", "")
    data_set = data_set.replace(" ", "")

    # The first step is to load the dataset using the sklearn toolkit.

    if data_set == "iris":
        from sklearn.datasets import load_iris
        raw_data = load_iris()
    elif data_set == "breast_cancer":
        from sklearn.datasets import load_breast_cancer
        raw_data = load_breast_cancer()
    elif data_set == "digits":
        from sklearn.datasets import load_digits
        raw_data = load_digits()
    elif data_set == "wine":
        from sklearn.datasets import load_wine
        raw_data = load_wine()
    else:
        print("Script not able to work with that data set")
        sys.exit(1)
    input_attr = raw_data['data']
    target_values = raw_data['target']
    print("Full data set is loaded.")

    return input_attr, target_values


def reduce_data_set(input_attr, target_values, reduction_list):
    """
    This function is responsible for reducing the dataset input attributes and target values
    so that we can use a binary perceptron ml model
    :param input_attr: numpy array of input attributes (full set)
    :param target_values:  numpy array of target values (full set)
    :param reduction_list: e.g. [1,2] list of values that can be used with a logical OR to reduce data set
    :return: reduced_x, reduced_y
    """
    # Since we want to create a binary predictor, we can use the numpy model, to reduce the size of the dataset.
    # We are going to consider examples that have a target value of 1 or 2. I will use the logical or from the numpy
    # library that we learned in the tutorials
    if len(reduction_list) != 2:
        print("Error, you need to enter an array with two numbers")
        sys.exit(1)
    else:
        first_target = sorted(reduction_list)[0]
        second_target = sorted(reduction_list)[1]

    reduction_matrix = np.logical_or(target_values == first_target, target_values == second_target)
    # Now we apply this to the input attribute and target arrays to reduce their size
    reduced_x = input_attr[reduction_matrix]
    # This eliminates the extra rows in the input x variable, but we need to also reduced the columns.
    reduced_x = reduced_x[:, first_target:second_target + 1]
    reduced_y = target_values[reduction_matrix]

    # We want to check if the input data rows, matches the output data values - to make sure the dot product
    # multiplication is possible when we get to training.
    if np.shape(reduced_x)[0] != np.shape(reduced_y)[0]:
        print("Error - The data set input and target shapes do not match. ")
        sys.exit(1)
    else:
        print("Dataset shapes match. Proceeding...")

    # To make the problem easier, we are going to map the two outputs to either a value of 1 or -1.
    # We can do this using the reduction list values.
    reduced_y[reduced_y == first_target] = 0
    reduced_y[reduced_y == second_target] = 1

    return reduced_x, reduced_y


def plot_input_vs_target(input_x, target_y):
    """
    This function takes the input attributes for a linear problem and plots them using the target values as the label.

    :param input_x: input attributes to plot (reduced inputs)
    :param target_y: target values to use as labels (reduced targets)
    :return: True on completion
    """
    # Determine the maximum values from each column in the matrix so we cn set the bounds of the plot:
    xy_plot_max = input_x.max(axis=0)
    xy_plot_min = input_x.min(axis=0)
    max_plot_x_value = xy_plot_max[0]
    max_plot_y_value = xy_plot_max[1]
    min_plot_x_value = xy_plot_min[0]
    min_plot_y_value = xy_plot_min[1]

    # Plot formatting
    plt.rcParams['font.size'] = 12
    plt.figure(figsize=(8, 8))

    # Plot each point as the label
    for x1, x2, label in zip(input_x[:, 0], input_x[:, 1], target_y):
        plt.text(x1, x2, str(label), fontsize=10, color='b',
                 ha='center', va='center')

    plt.xlim((min_plot_x_value, max_plot_x_value))
    plt.ylim((min_plot_y_value, max_plot_y_value))
    plt.xlabel('x1', size=20)
    plt.ylabel('x2', size=20)
    plt.title('Data Plot', size=24)
    plt.show()

    return True

def adjust_b_cancer_data():
    """
    This function is responsible for modelling the breast cancer dataset into an appropriate
     format for use with the Perceptron algorithm.
    :return: x_train, x_test, y_train, y_test
    """

    # load the breast cancer data
    from sklearn.datasets import load_breast_cancer
    breast_cancer = load_breast_cancer()

    # convert the data to pandas dataframe.
    input_data = breast_cancer.data
    target_data = breast_cancer.target

    data = pd.DataFrame(breast_cancer.data, columns=breast_cancer.feature_names)
    data['class'] = breast_cancer.target
    data.groupby('class').mean()

    # perform scaling on the data.
    input_data = data.drop("class", axis=1)
    target_data = data["class"]

    # train test split.
    X_train, X_test, Y_train, Y_test = train_test_split(input_data, target_data, test_size=0.1, stratify=target_data, random_state=1)

    X_train = X_train.values
    X_test = X_test.values

    return X_train, X_test, Y_train, Y_test

class Perceptron_Model:
    """
    This class will contain the inner workings of a perceptron model, used to determine
    the weighting of a model based in input attributes and output targets.
    """

    def __init__(self):
        """
        Establish variables for the initialisation of the class.
        Set variables to be used globally within class to starting value of None
        """
        self.weights = None
        self.bias = None

    def model(self, input_x):
        """
        Calculate the dot product of weights and input_attributres
        :param input_x:
        :return: 1 if dot product is greater or equal to bias, 0 otherwise
        """

        dot_prod = np.dot(self.weights, input_x)
        if dot_prod >= self.bias:
            return 1
        else:
            return 0

    def prediction(self, input_attrs):
        """
        Loop through each input attribute(x) in a list of input attributes and
        feed to model function to determine if the dot is greater than bias
        :param input_attrs:
        :return: numpy array of target predictions
        """
        predicted_Y = []
        for x in input_attrs:
            bias_compare = self.model(x)
            predicted_Y.append(bias_compare)
        return np.array(predicted_Y)

    def perform_fit(self, X, Y, epochs=1, lr=0.5):
        """
        This function will loop through a number or epochs/specified training loops
        and for each epoch, check if the predicted Y/target, matches the actual target.
        if it does not, it will adjust the bias for the function accordingly.
        :param X: Input attributes
        :param Y: target attributes
        :param epochs: Number of training loops
        :param lr: learning rate, a float value between 0 and 1
        :return: numpy array weighted matrix
        """
        self.weights = np.zeros(X.shape[1])
        self.bias = 0

        # initialise vars
        weight_matrix = []
        accuracy_counter = {}
        max_accuracy = 0

        # For each learning cycle
        for i in range(epochs):
            # loop through attributes
            for x, y in zip(X, Y):
                predicted_y = self.model(x)
                if y == 0 and predicted_y == 1:
                    self.weights = self.weights - lr * x
                    self.bias = self.bias + lr * 1
                elif y == 1 and predicted_y == 0:
                    self.weights = self.weights + lr * x
                    self.bias = self.bias - lr * 1

            weight_matrix.append(self.weights)
            accuracy_counter[i] = accuracy_score(self.prediction(X), Y)

            # Set loop to store the maximum accuracy over the epoch loops so that
            # we have a mechanism to save those weights and bias.
            if (accuracy_counter[i] > max_accuracy):
                max_accuracy = accuracy_counter[i]
                saved_iteration_number = i
                saved_weights = self.weights
                saved_bias = self.bias

            #Adding catch to stop learning when algorithm has converged.
            if max_accuracy == 1.0:
                print("Solution has converged, stopping training")
                break

        self.weights = saved_weights
        self.bias = saved_bias

        result_weights = np.array(weight_matrix)

        self.plot_epoch_results(accuracy_counter, saved_iteration_number, max_accuracy)

        return result_weights

    def plot_epoch_results(self, accuracy_counter, saved_iteration_number, max_accuracy):
        """
        This function is used to plot the results of the training over the
        different epoch iterations
        :param accuracy_counter: the list of accuracy scores calculated
        :param saved_iteration_number: the index of the best result
        :param max_accuracy: The best accuracy score from the learning cycles
        """
        print(max_accuracy, saved_iteration_number)
        # print(accuracy.values())
        plt.plot(list(accuracy_counter.values()))
        plt.xlabel("Iteration/Epoch #")
        plt.ylabel("Accuracy")
        plt.ylim([0, 1])
        plt.show()
        print("Max Accuracy Achieved on Epoch: " + str(saved_iteration_number) + " with value: " + str(max_accuracy))
        # stored calculated weights in   numpy array
        return True

if __name__ == "__main__":
    print("Running Perceptron Model code file")
    print("What data model would you like to load for training?\n")
    data_selection = input("'iris' or 'breast_cancer'\n")
    input_x, input_y = load_training_data(data_selection)

    if data_selection == "iris":
        reduce_dataset = True
        print("Reducing size of Iris dataset to only include linearly separable attributes. ")
        if reduce_dataset:
            input_x, input_y = reduce_data_set(input_x, input_y, [0, 1])
            train_x, test_x, train_y, test_y = train_test_split(input_x, input_y, test_size=0.1,
                                                                stratify=input_y, random_state=1)
    elif data_selection == "breast_cancer":
        train_x, test_x, train_y, test_y = adjust_b_cancer_data()

    print("The shape of the input attribute dataset is: " + str(np.shape(train_x)))

    print("The shape of the target output dataset is: " + str(np.shape(train_y)))
    # now we have our filtered data, we want to create a split of data that can be used for training,
    # and validation. We will use the sklearn to do this. This will give us 4 datasets

    # Optional: plot the data to check if it is linear.
    # For a perceptron model to work, we need to visually identify if there is a hyperplane that can split the
    # data points. This means that the data is linearly separable and appropriate for a Perceptron ML model.
    perform_plot = False
    if perform_plot:
        print("Plotting values using matplotlib")
        plot_input_vs_target(train_x, train_y)

    # load Perceptron class
    perceptron = Perceptron_Model()
    model_weights = perceptron.perform_fit(train_x, train_y, 10000, 0.1)

    # Do comparison between trained model
    # Note: weights have been set in the class after training.
    print("Now use stored weights from most accurate training iteration with test"
          " dataset")
    y_predicted = perceptron.prediction(test_x)

    print("Accuracy Score using test data:")
    print(accuracy_score(y_predicted, test_y))

